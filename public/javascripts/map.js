var map = L.map('main_map').setView([8.960641, -75.454713], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
}).addTo(map);

L.marker([8.959304, -75.452935]).addTo(map);
L.marker([8.956930, -75.452098]).addTo(map);
L.marker([8.957820, -75.455199]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bycyclets",
    success: function(response){
        console.log(response);
        response.bycyclets.forEach(function(data){
            L.marker(data.ubication, {title: data.id}).addTo(map);
        });
    }
});