var Bycycle = require('../../models/bycycle');
var request = require('request');
var server = require('../../bin/www');



describe('BycycleAPI', () => {
    describe('GET Bycyclets', () => {
        it('Status 200', () => {
            expect(Bycycle.allBycycles.length).toBe(0);

            var a = new Bycycle(6, "Rojo", "Montaña", [8.957820, -75.455199]);
            Bycycle.add(a);

            request.get('http://localhost:3000/api/bycyclets', function (err, resp, body) {
                expect(resp.statusCode).toBe(200);
            });
        });
    });

    describe('POST Bycycle/create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' }
            var bycycle = { "id": 6, "color": "Red", "model": "City", "lat": 8.957820, "lng": -75.455199 };
            request.post({
                headers: headers,
                url: 'http://localhost:3000/bycyclets/create',
                body: bycycle
            }, function (err, res, body) {
                expect(res.statusCode).toBe(200);
                expect(Bycycle.findById(6).color).toBe("Red");
                done();
            });
        });
    });
});