var mongoose = require('mongoose');
var Bycycle = require('../../models/bycycle');
var User = require('../../models/user');
var Reservation = require('../../models/reservation');

describe('Test Users', function(){
    beforeEach(function(done){
        mongoose.set('useNewUrlParser', true);
        mongoose.set('useFindAndModify', false);
        mongoose.set('useCreateIndex', true);
        mongoose.set('useUnifiedTopology', true);
        mongoose.connect('mongodb://localhost/testdb');

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reservation.deleteMany({}, function(err, success){
            if(err) console.log(err);
            User.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Bycycle.deleteMany({}, function(err, success){
                   if(err) console.log(err);
                   done();
                });
            })
        });
    });


});

describe('Where user reserve one bycycle', () => {
    it('the reservation must exist', (done) => {
        const user = new User({name: 'Other'});
        user.save();
        const bycycle = new Bycycle({code: 2, model: 'Urban'});
        bycycle.save();

        var today = new Date();
        var tomorrow = new Date();
        tomorrow.setDate(today.getDate()+1);
        user.reservation(bycycle.id, today, tomorrow, function(err, reserv){
            Reservation.find({}).populate('bycycle').populate('user').exec(function(err, reservations){
                if(err) console.log(err);
                console.log(reservations[0]);
                expect(reservations.length).toBe(1);
                expect(reservations[0].daysReservation()).toBe(2);
                expect(reservations[0].bycycle.code).toBe(2);
                expect(reservations[0].user.name).toBe(user.name);
                done();
            }).catch(error =>{
                console.log(error);
            });
        });
    });
});