var mongoose = require('mongoose');
const Bycycle = require('../../models/bycycle');

//Test with mongo
describe('Test Bycycle', function(){
    beforeEach(function(done){
        mongoose.set('useNewUrlParser', true);
        mongoose.set('useFindAndModify', false);
        mongoose.set('useCreateIndex', true);
        mongoose.set('useUnifiedTopology', true);
        mongoose.connect('mongodb://localhost/testdb');

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bycycle.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bycycle.createInstance', () => {
        it('Create instance of Bycycle', () => {
            var bycycle = Bycycle.createInstance(1, "Green", "Urban", [-4555, -4555]);
            expect(bycycle.code).toBe(1);
            expect(bycycle.color).toBe("Green");
            expect(bycycle.model).toBe("Urban");
            expect(bycycle.ubication[0]).toBe(-4555);
            expect(bycycle.ubication[1]).toBe(-4555);
        });
    });

    describe('Bycycle.allBycycles', () => {
        it('Start Empty', (done) => {
            Bycycle.allBycycles(function(err, bycyclets){
                expect(bycyclets.length).toBe(0);
                done();
            });
        });
    });

    describe('Bycycle.add', () => {
        it('Add only bycycle', (done) => {
            var mBycycle = new Bycycle({code: 1, color: "Green", model: "Urban"});
            Bycycle.add(mBycycle, function(err, newBycy){
                if(err) console.log(err);
                Bycycle.allBycycles(function(err, bycys){
                    expect(bycys.length).toEqual(1);
                    expect(bycys[0].code).toEqual(mBycycle.code);

                    done();
                });
            });
        });
    });

    describe('Bycycle.findByCode', () => {
        it('Return bycycle with code', (done) => {
            Bycycle.allBycycles(function(err, bycys){
                expect(bycys.length).toBe(0);

                var mBycy = new Bycycle({code: 1, color: "Green", model: "Urban"});
                Bycycle.add(mBycy, function(err, newBycy){
                    if (err) console.log(err);

                    var aBycycle = new Bycycle({code: 2, color: "Red", model: "Urban"});
                    Bycycle.add(aBycycle, function(err, newBycy){
                        if(err) console.log(err);
                        Bycycle.findByCode(1, function(error, targetBycycle){
                            expect(targetBycycle.code).toBe(mBycy.code);
                            expect(targetBycycle.color).toBe(mBycy.color);
                            expect(targetBycycle.model).toBe(mBycy.model);

                            done();
                        });
                    });
                });
            });
        });
    });

});

//Testing without mongo
/*beforeEach(() => Bycycle.allBycycles = []);

//Testing group
describe('Bycycle.allBycyclets', () => {
    it('Start empty', () => {
        expect(Bycycle.allBycycles.length).toBe(0);
    });
});

describe('Bycycle.add', () => {
    it('Add One', () => {
        //Check precondition if empty
        //expect(Bycycle.allBycycles.length).toBe(0);
        //Create Bycycle
        var a = new Bycycle(1, "Rojo", "Montaña", [8.957820, -75.455199]);
        Bycycle.add(a);
        //Check if add
        expect(Bycycle.allBycycles.length).toBe(1);
        //Check if bycycle is same 
        expect(Bycycle.allBycycles[0]).toBe(a);
    });
});

describe('Bycycle.findById', () => {
    it('Return Bycycle with Id: 1', () => {
        expect(Bycycle.allBycycles.length).toBe(0);
        var a = new Bycycle(1, "Rojo", "Montaña", [8.957820, -75.455199]);
        var b = new Bycycle(2, "Green", "Urban", [8.957820, -75.455199]);
        Bycycle.add(a);
        Bycycle.add(b);
        var targetBycyclet = Bycycle.findById(1);
        expect(targetBycyclet.id).toBe(1);
        expect(targetBycyclet.color).toBe(a.color);
        expect(targetBycyclet.model).toBe(a.model);
    });
});*/