var Bycycle = require('../../models/bycycle')

exports.bycycle_list = function(req, res){
    res.status(200).json({
        bycyclets: Bycycle.allBycycles
    });
}

exports.bycycle_create = function(req, res){
    var bycycle = new Bycycle(req.body.id, req.body.color, req.body.model);
    bycycle.ubication = [req.body.lat, req.body.lng];
    Bycycle.add(bycycle); 
    res.status(200).json({
        bycycle: bycycle
    });
}

exports.bycycle_delete = function(req, res){
    Bycycle.remove(req.body.id);
    res.status(204).send();
}

exports.bycycle_update = function(req, res){
    try{
        var bycycle = Bycycle.findById(req.body.id);
        bycycle.color = req.body.color;
        bycycle.model = req.body.model;
        bycycle.ubication = [req.body.lat, req.body.lng];
        res.status(200).json({
            bycycle: bycycle
        });
    }catch(e){
        res.status(200).json({
            error: e.message
        });
    }
}