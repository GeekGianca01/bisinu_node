var User = require('../../models/user');

exports.users_list = function(req, res){
    User.find({}, function(err, users){
        res.status(200).json({
            users: users
        });
    });
};

exports.user_create = function(req, res){
    var user = new User({name: req.body.name});
    user.save(function(err){
        res.status(200).json(user);
    });
};

exports.user_reservation = function(req, res){
    User.findById(req.body.id, function(err, user){
        console.log(user);
        user.reservation(req.body.bycycle_id, req.body.since, req.body.until, function(err){
            console.log('Reservation !!!');
            res.status(200).send();
        });
    });
};