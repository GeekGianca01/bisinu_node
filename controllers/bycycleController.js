var Bycycle = require('../models/bycycle');

exports.bycycle_list = function(req, res){
    res.render('bycyclesView/index', {bycys: Bycycle.allBycycles});
}

exports.bycycle_create_get = function(req, res){
    res.render('bycyclesView/create', {bycys: Bycycle.allBycycles});
}

exports.bycycle_create_post = function(req, res){
    var bycycle = new Bycycle(req.body.idInput, req.body.colorInput, req.body.modelInput);
    bycycle.ubication = [req.body.latInput, req.body.lngInput];
    Bycycle.add(bycycle);
    res.redirect('/bycyclets');
}

exports.bycycle_update_get = function(req, res){
    var bycycle = Bycycle.findById(req.params.id);
    res.render('bycyclesView/update', {bycycle});
}

exports.bycycle_update_post = function(req, res){
    console.log(`Search params ${req.params.id}`);
    var bycycle = Bycycle.findById(req.params.id);
    bycycle.id = req.body.idInput;
    bycycle.color = req.body.colorInput;
    bycycle.model = req.body.modelInput;
    bycycle.ubication = [req.body.latInput, req.body.lngInput];
    res.redirect('/bycyclets');
}

exports.bycycle_delete_post = function(req, res){
    Bycycle.remove(req.body.id);
    res.redirect('/bycyclets');
}