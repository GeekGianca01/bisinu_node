//Model with mongo
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bycycleSchema = new Schema({
    code: Number,
    color: String,
    model: String,
    ubication: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    }
});

bycycleSchema.statics.createInstance = function (code, color, model, ubication) {
    return new this({
        code: code, 
        color: color, 
        model: model, 
        ubication: ubication
    });
}

bycycleSchema.methods.toString = function () {
    return "Code: " + this.code + " | Color: " + this.color;
}

bycycleSchema.statics.allBycycles = function (cb) {
    return this.find({}, cb);
}

bycycleSchema.statics.add = function(mBycycle, cb){
    this.create(mBycycle, cb);
}

bycycleSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
}

bycycleSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code: aCode}, cb);
}

module.exports = mongoose.model('Bycycle', bycycleSchema);

//Model without Mongo
/*var Bycycle = function (id, color, model, ubication) {
    this.id = id;
    this.color = color;
    this.model = model;
    this.ubication = ubication;
}

Bycycle.prototype.toString = function () {
    return "Color: " + this.color + " | Modelo: " + this.model;
}

Bycycle.allBycycles = [];

Bycycle.add = function (allBycycles) {
    Bycycle.allBycycles.push(allBycycles);
}

Bycycle.findById = function (idBycycle) {
    var mBycycle = Bycycle.allBycycles.find(x => x.id == idBycycle);
    if (mBycycle)
        return mBycycle;
    else
        throw new Error(`No existe una bicicleta con el id ${idBycycle}`);
}

Bycycle.remove = function (mBycycle) {
    for (var i = 0; i < Bycycle.allBycycles.length; i++) {
        console.log(`Searching...${mBycycle}`);
        if (Bycycle.allBycycles[i].id == mBycycle) {
            Bycycle.allBycycles.splice(i, 1);
            break;
        }
    }
}

/*var a = new Bycycle(1, "Rojo", "Montaña", [8.957820, -75.455199]);
var b = new Bycycle(2, "Gris", "Urbana", [8.957820, -75.455199]);
var c = new Bycycle(3, "Verde", "Clasica", [8.957820, -75.455199]);
var d = new Bycycle(4, "Azul", "Montaña", [8.957820, -75.455199]);

Bycycle.add(a);
Bycycle.add(b);
Bycycle.add(c);
Bycycle.add(d);

module.exports = Bycycle;*/