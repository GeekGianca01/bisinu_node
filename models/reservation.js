var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var reservationSchema = new Schema({
    since: Date, 
    until: Date,
    bycycle: {type: mongoose.Schema.Types.ObjectId, ref: 'Bycycle'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
});

reservationSchema.methods.daysReservation = function(){
    return moment(this.until).diff(moment(this.since), 'days') + 1;
};

module.exports = mongoose.model('Reservation', reservationSchema);