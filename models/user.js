var mongoose = require('mongoose');
var Reservation = require('./reservation');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: String
});

userSchema.methods.reservation = function(bycycleId, since, until, callback){
    var reservation = new Reservation({user: this._id, bycycle: bycycleId, since: since, until: until});
    console.log(reservation);
    reservation.save(callback);
};

module.exports = mongoose.model('User', userSchema);