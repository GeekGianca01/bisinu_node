# Bienvenido a Express

Proyecto de bicicletas publicas de Bisinu
Project generator with Express used for this project.
Global installation
```sh
npm install express-generator -g
```

Then the generated express folders for the project are created.
```sh
express --view=pug #name_project
```
Comparison image with original server
![Comparison image with original server](server.jpg)

Map service with markers
![Map service with markers](map-in-web.jpg)

HTTP services
GET
![HTTP services](postman-get.jpg)

POST
![HTTP services](postman-post.jpg)

PUT
![HTTP services](postman-put.jpg)

DELETE
![HTTP services](postman-delete.jpg)