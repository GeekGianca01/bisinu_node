var express = require('express');
var router = express.Router();
var bycycletController = require('../../controllers/api/bycycleControllerAPI');
const { route } = require('../bycyclets');

router.get('/', bycycletController.bycycle_list);
router.post('/create', bycycletController.bycycle_create);
router.put('/update', bycycletController.bycycle_update);
router.delete('/delete', bycycletController.bycycle_delete);

module.exports = router;