var express = require('express');
var router = express.Router();
var bycycletController = require('../controllers/bycycleController');

router.get('/', bycycletController.bycycle_list);

router.get('/create', bycycletController.bycycle_create_get);
router.post('/create', bycycletController.bycycle_create_post);
router.get('/:id/update', bycycletController.bycycle_update_get);
router.post('/:id/update', bycycletController.bycycle_update_post);
router.post('/:id/delete', bycycletController.bycycle_delete_post);

module.exports = router;